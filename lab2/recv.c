#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

int main(int argc,char** argv){
  msg r,t;
  init(HOST,PORT);

  int fd;

  recv_message(&r);
  if(r.payload[0] == 'N')
    fd = open(r.payload, O_CREAT | O_RDWR, 0677);

  while(r.payload[0] != 'E'){

  if (recv_message(&r)<0){
    perror("Receive message");
    return -1;
  }

  printf("[%s] Got msg with payload: %c\n",argv[0],r.payload[0]);

    if(r.payload[0] == 'D'){
      write(fd, r.payload + 1, r.len - 1);
    }
  }

if(fd < 0)
  sprintf(t.payload,"X%s",r.payload);
  t.len = strlen(t.payload) + 1;
  send_message(&t);
  close(fd);
  /*
  sprintf(t.payload,"ACK(%s)",r.payload);
  t.len = strlen(t.payload) + 1;
  send_message(&t);*/
  return 0;
}
