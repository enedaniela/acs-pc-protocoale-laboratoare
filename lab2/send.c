#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10000
#define DIM 1399

int main(int argc,char** argv){
  init(HOST,PORT);
  msg t;

  int fd;

  /*sprintf(t.payload,"Hello World of PC");
  t.len = strlen(t.payload)+1;
  send_message(&t);*/

//trimit numele fisierului
  sprintf(t.payload, "N%s", argv[1]); 
  t.len = strlen(t.payload) + 1;
  send_message(&t);

char buf[1400];
int copiat;
//incep sa trimit date
  fd = open(argv[1], O_RDONLY);
  while((copiat = read(fd, t.payload+1, DIM))){
    if (copiat < 0){
        perror("Eroare la citire");
        exit(1);
      }

    t.payload[0] = 'D';
    t.len = copiat + 1;
    send_message(&t);
  }
  //trimit mesaj final
  t.payload[0] = 'E';
  t.len = 1;
  send_message(&t);
  close(fd);

  if (recv_message(&t)<0){
    perror("receive error");
  }
  else {
    printf("[%s] Got reply with payload: %s\n",argv[0],t.payload);
  }

  return 0;
}
