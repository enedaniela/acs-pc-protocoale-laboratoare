#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

char compute_bits(msg *h){
  char sum = 0;
  int i, j;
  //check sum 
  for(i = 0; i < h->len; i++){
    for(j = 0; j < 8; j++)
      sum ^= (h->payload.date[i] >> j) & 1;
  }
    return sum;
}

int main(int argc,char** argv){
  msg r,t;
  init(HOST,PORT);
  int ok = 0;
  int fd;

  recv_message(&r);

  if(r.payload.check == compute_bits(&r))
    ok = 1;
  if(ok && r.payload.type == 'N')
    fd = open("Fisier.pdf", O_CREAT | O_RDWR, 0677);
  else
  printf("Eroare la transmitere");

  while(r.payload.type != 'E'){

    if (recv_message(&r)<0){
      perror("Receive message");
      return -1;
    }
    ok = 0;
    if(r.payload.check == compute_bits(&r))
      ok = 1;
    if(!ok)
      printf("\nEroare la trasmitere\n");

    printf("[%s] Got msg with payload: %c\n",argv[0],r.payload.type);

      if(ok && r.payload.type == 'D'){
        write(fd, r.payload.date, r.len - 2);
      }
        sprintf(t.payload.date,"ACK(%s)",r.payload.date);
  t.payload.type = 'A';
  t.len = strlen(t.payload.date) + 1;
  send_message(&t);


  }

if(fd < 0){
  t.payload.type = 'X';
  send_message(&t);
}
  close(fd);
  
  return 0;
}
