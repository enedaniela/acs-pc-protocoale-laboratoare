#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10000
#define DIM 1398

char compute_bits(msg *h){
  char sum = 0;
  int i, j;
  //check sum 
  for(i = 0; i < h->len; i++){
    for(j = 0; j < 8; j++)
      sum ^= (h->payload.date[i] >> j) & 1;
  }
    return sum;
}

int main(int argc,char** argv){
  init(HOST,PORT);
  msg t;
  int fd;


//trimit numele fisierului
  t.payload.type = 'N';
  sprintf(t.payload.date,"%s", argv[1]);

  t.len = strlen(t.payload.date);
  t.payload.check = compute_bits(&t);
  send_message(&t);

int copiat;
//incep sa trimit date
  fd = open(argv[1], O_RDONLY);
  while((copiat = read(fd, t.payload.date, DIM))){
    if (copiat < 0){
        perror("Eroare la citire");
        exit(1);
      }

    t.payload.type = 'D';
    t.len = copiat + 2 * sizeof(char);
    t.payload.check = compute_bits(&t);
    send_message(&t);
      if (recv_message(&t)<0){
    perror("receive error");
  }
  else {
    printf("[%s] Got reply with payload: %c\n",argv[0],t.payload.type);
  }
  }

  //trimit mesaj final
  t.payload.type = 'E';
  t.len = 2;
  t.payload.check = compute_bits(&t);
  send_message(&t);
  close(fd);



  return 0;
}
