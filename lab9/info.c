#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
/*
struct hostent
{
 char *h_name;
 char **h_aliases;
 int h_addrtype;
 int h_length;
 char **h_addr_list;*/

void showInfo(struct hostent h){
	int i = 0;

	printf("Host name: %s\n", h.h_name);
	while(h.h_aliases[i]!=NULL){
		printf("%s\n",h.h_aliases[i]);
		i++;
	}
	printf("Address type: %d\n",h.h_addrtype);
	printf("Address length: %d\n",h.h_length);
	i = 0;

	while(h.h_addr_list[i] != NULL){
		printf("%s\n",inet_ntoa(*(struct in_addr*)h.h_addr_list[i]));
		i++;
	}
}

int main(int argc, char *argv[]){
	struct hostent* h;
	struct in_addr aux;
	if(argv[1][0] == 'n')
		h = gethostbyname(argv[2]);

	if(argv[1][0] == 'a'){
		inet_aton(argv[2],&aux);
		h = gethostbyaddr(&aux, 4, AF_INET);
	}

	int i = 0;
	if(h!=NULL){
	printf("Host name: %s\n", h->h_name);
	while(h->h_aliases[i]!=NULL){
		printf("%s\n",h->h_aliases[i]);
		i++;
	}
	printf("Address type: %d\n",h->h_addrtype);
	printf("Address length: %d\n",h->h_length);
	i = 0;

	while(h->h_addr_list[i] != NULL){
		printf("%s\n",inet_ntoa(*(struct in_addr*)h->h_addr_list[i]));
		i++;
	}
}
else
	herror("Error");
}