/*
*  	Protocoale de comunicatii: 
*  	Laborator 7: TCP
*/

#include "helpers.h"
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


void usage(char*file)
{
	fprintf(stderr,"Usage: %s ip_server port_server file\n",file);
	exit(0);
}

int main(int argc,char**argv)
{
	/*if (argc!=2)
		usage(argv[0]);*/
	
	int sd;
	struct sockaddr_in to_station;
	char buf[BUFLEN];
	int n;

	/*Deschidere socket*/
	sd = socket(PF_INET, SOCK_STREAM, 0);
	//printf("Descriptor socket %d\n", sd);
	if(sd < 0)
	{
		
		perror("Problema socket\n");
		return -2;
	}
	
	/*Setare struct sockaddr_in pentru a specifica unde trimit datele*/
	to_station.sin_family = AF_INET;
	inet_aton(argv[1], &(to_station.sin_addr));
	to_station.sin_port = htons(atoi(argv[2]));
 	
	int c = connect(sd, (struct sockaddr *) &to_station, sizeof(struct sockaddr));

	if(c != 0){
		perror("Error connecting..\n");
	}

	while(1)
	{	
		n = recv(sd, buf, BUFLEN, 0);
		write(1,buf,n);

		n = read(0, buf, BUFLEN - 1);
		buf[n] = 0;
		if(n <= 0)
		{
			//printf("Problema la citire\n");
			break;
		}		
		send(sd, buf, n + 1, 0);
	
		if(buf[0] == 'x')
			break;
	}
	/*Inchidere socket*/
	close(sd);

	printf("\n");
	return 0;
}
