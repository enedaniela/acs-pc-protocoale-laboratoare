/*
*  	Protocoale de comunicatii: 
*  	Laborator 7: TCP
*	mini-server de backup fisiere
*/

#include "helpers.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>



void usage(char*file)
{
	fprintf(stderr,"Usage: %s server_port file\n",file);
	exit(0);
}

/*
*	Utilizare: ./server server_port nume_fisier
*/
int main(int argc,char**argv)
{
	char buff[BUFLEN];
	/*if (argc!=2)
		usage(argv[0]);*/
	
	struct sockaddr_in my_sockaddr,from_station ;

	/*Setare struct sockaddr_in pentru a asculta pe portul respectiv */
	my_sockaddr.sin_family = AF_INET;
	inet_aton(argv[1], &(my_sockaddr.sin_addr));
	my_sockaddr.sin_port = htons(atoi(argv[2]));

	from_station.sin_family = AF_INET;
	from_station.sin_addr.s_addr = htonl(INADDR_ANY);
	from_station.sin_port = htons(0);

	/*Deschidere socket*/
	int socketfd = socket(PF_INET, SOCK_STREAM, 0);
	if(socketfd < 0)
		return -1;
	
	/* Legare proprietati de socket */
	int b;
	b = bind(socketfd, (struct sockaddr *) &my_sockaddr, sizeof(struct sockaddr));
	if(b < 0)
		return -1;
	
	listen(socketfd, 5);
	int n1 = sizeof(struct sockaddr);
	int sa = accept(socketfd, (struct sockaddr *) &from_station, &n1);

	int n;

	while(1){
		n = recv(sa, buff, BUFLEN, 0);

		write(1, buff, n);

		send(sa,buff,n,0);

		if(buff[0] == 'x')
			break;
	}

	/*Inchidere socket*/
	close(socketfd);
	close(sa);	

	return 0;
}
