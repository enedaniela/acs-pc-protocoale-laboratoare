/*
*  	Protocoale de comunicatii: 
*  	Laborator 7: TCP
*	server chat
*/

#include "helpers.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc,char**argv)
{
	char buff[BUFLEN];
	
	struct sockaddr_in my_sockaddr,from_station ;

	/*Setare struct sockaddr_in pentru a asculta pe portul respectiv */
	my_sockaddr.sin_family = AF_INET;
	inet_aton(argv[1], &(my_sockaddr.sin_addr));
	my_sockaddr.sin_port = htons(atoi(argv[2]));

	from_station.sin_family = AF_INET;
	from_station.sin_addr.s_addr = htonl(INADDR_ANY);
	from_station.sin_port = htons(0);

	/*Deschidere socket*/
	int socketfd = socket(PF_INET, SOCK_STREAM, 0);
	if(socketfd < 0)
		return -1;
	
	/* Legare proprietati de socket */
	int b;
	b = bind(socketfd, (struct sockaddr *) &my_sockaddr, sizeof(struct sockaddr));
	if(b < 0)
		return -1;
	
	listen(socketfd, 5);

	int n1 = sizeof(struct sockaddr);
	int sa1 = accept(socketfd, (struct sockaddr *) &from_station, &n1);
	int sa2 = accept(socketfd, (struct sockaddr *) &from_station, &n1);

	int n;
	while(1){
		n = recv(sa1, buff, BUFLEN, 0);//primesc de le primul
		write(1, buff, n);
		send(sa2,buff,n,0);// trimit celui de-al doilea

		n = recv(sa2, buff, BUFLEN, 0);//primesc de la cel de-al doilea
		write(1, buff, n);
		send(sa1,buff,n,0);//primesc de la primul

		if(buff[0] == 'x')
			break;
	}

	/*Inchidere socket*/
	close(socketfd);
	close(sa1);
	close(sa2);	

	return 0;
}
