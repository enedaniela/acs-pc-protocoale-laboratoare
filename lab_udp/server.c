/*
*  	Protocoale de comunicatii: 
*  	Laborator 6: UDP
*	mini-server de backup fisiere
*/

#include "helpers.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>



void usage(char*file)
{
	fprintf(stderr,"Usage: %s server_port file\n",file);
	exit(0);
}

/*
*	Utilizare: ./server server_port nume_fisier
*/
int main(int argc,char**argv)
{
	char buff[BUFLEN];
	if (argc!=3)
		usage(argv[0]);
	
	struct sockaddr_in my_sockaddr,from_station ;
//	char buf[BUFLEN];
	/*Setare struct sockaddr_in pentru a asculta pe portul respectiv */
	my_sockaddr.sin_family = AF_INET;
	inet_aton("172.16.4.20", &(my_sockaddr.sin_addr));
	my_sockaddr.sin_port = htons(atoi(argv[1]));

	from_station.sin_family = AF_INET;
	from_station.sin_addr.s_addr = htonl(INADDR_ANY);
	from_station.sin_port = htons(0);

	/*Deschidere socket*/
	int sockfd = socket(PF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0)
		return -1;
	
	/* Legare proprietati de socket */
	int b;
	b = bind(sockfd, (struct sockaddr *) &my_sockaddr, sizeof(struct sockaddr));
	if(b < 0)
		return -1;
	int fd;
	/* Deschidere fisier pentru scriere */
	DIE((fd=open(argv[2],O_WRONLY|O_CREAT|O_TRUNC,0644))==-1,"open file");
	
	/*
	*  cat_timp  mai_pot_citi
	*		citeste din socket
	*		pune in fisier
	*/
	int n;
	int n1 = sizeof(struct sockaddr);
	while(1){
		n = recvfrom(sockfd, buff, BUFLEN, 0, (struct sockaddr *)&from_station, (socklen_t *) &n1);
		if(n > 0)
			printf("#");
		write(fd, buff, n);
		if(n < BUFLEN)
			break;

	}

	/*Inchidere socket*/
	close(sockfd);	
	
	/*Inchidere fisier*/
	close(fd);

	return 0;
}
